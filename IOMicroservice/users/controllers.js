const Router = require('express').Router();

const {
    getUsers,
    getUserById,
    createUser,
    deleteUser,
    updateUser,
	authenticate
} = require('./services.js');

Router.get('/', async (req, res) => {
    const users = await getUsers();
    res.json(users);
});

Router.get('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const user = await getUserById(id);
    res.json(user);
});

Router.put('/:id', async (req, res) => {
    const {
        username,
		password,
		role,
        cash
    } = req.body;

    const {
        id
    } = req.params;

    const userId = await updateUser(id, username, password, role, cash);
    res.json(userId);
});

Router.post('/', async (req, res) => {
    const {
        username,
		password,
		role,
        cash
    } = req.body;

    const userId = await createUser(username, password, role, cash);
    res.json(userId);
});

Router.post('/login', async (req, res) => {
    const {
        username,
		password
    } = req.body;

    const token = await authenticate(username, password);
    res.json(token);
});

Router.delete('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const users = await deleteUser(id);
    res.json(users);
});

module.exports = Router;
