const {
    query
} = require('../data');

const {
    generateToken,
} = require('../SecurityUtils/Jwt');

const {
    hash,
    compare
} = require('../SecurityUtils/Password');

const getUsers = async () => {
    console.info(`Getting all users`);
    const users = await query("SELECT id, username, cash FROM users");
    return users;
};

const getUserById = async (id) => {
    console.info(`Getting user ${id}`);
    const users = await query("SELECT username, cash FROM users WHERE id = $1", [id]);
    return users[0];
};

const updateUser = async (id, username, password, role, cash) => {
    console.info(`Update user with id ${id}, with name ${username} and ${cash} $`);
	const encrypted = await hash(password);
    const users = await query("UPDATE users SET username = $1, password = $2, role = $3, cash = $4 where id = $5", [username, encrypted, role, cash, id]);
    return users;
};

const createUser = async (username, password, role, cash) => {
    console.info(`Adding user ${username}, with ${cash} $`);
	const encrypted = await hash(password);
    const users = await query("INSERT INTO users (username, password, role, cash) VALUES ($1, $2, $3, $4) RETURNING id", [username, encrypted, role, cash]);
    return users[0].id;
}

const authenticate = async (username, password) => {
    const result = await query('SELECT id, password, role FROM users WHERE username = $1', [username]);

    if (result.length === 0) {
        throw new ServerError('Utilizatorul cu username ${username} nu exista in sistem!', 400);
    }
    const user = result[0];
	console.log(`username: ${user.id}\nrole: ${user.role}\npassword: ${user.password}`);

    const passwordMatch = await compare(password, user.password);
    console.log(`passwordMatch: ${passwordMatch}`);

    if (!passwordMatch) {
        throw new Error('Parola gresita!');
    } else {
        console.log('Parola corecta!');
    }

    const token = await generateToken({
        userId: user.id,
        userRole: user.role
    });
    console.log(`token: ${token}`);

	return token;
};

const deleteUser = async (id) => {
    console.info(`Deleting user ${id}`);
    const users = await query("DELETE FROM users where id = $1", [id]);
    return users;
}

module.exports = {
    getUsers,
    getUserById,
    createUser,
    deleteUser,
    updateUser,
	authenticate
}
