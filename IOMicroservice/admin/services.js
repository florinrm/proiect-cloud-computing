const {
    query
} = require('../data');

const getBikes = async () => {
    console.info(`Getting all bikes ...`);

    const bikes = await query("SELECT id, bike_name, stock, price FROM bikes");

    return bikes;
};

const getBikeById = async (id) => {
    console.info(`Getting bike ${id} ...`);

    const bikes = await query("SELECT bike_name, stock, price FROM bikes WHERE id = $1", [id]);
    return bikes[0];
};

const updateBike = async (id, bike_name, stock, price) => {
    console.info(`Buying bike with id ${id}`);

    const bikes = await query("UPDATE bikes SET bike_name = $1, stock = $2, price  = $3 where id = $4", [bike_name, stock, price, id]);
    return bikes;
};

const deleteBike = async (id) => {
    console.info(`Deleting bike ${id}`);
    const users = await query("DELETE FROM bikes where id = $1", [id]);
    return users;
};

const addBike = async (bike_name, stock, price) => {
    console.info(`Adding bike ${bike_name}, with ${price} $ and no. of ${stock}`);
    const users = await query("INSERT INTO bikes (bike_name, stock, price) VALUES ($1, $2, $3) RETURNING id", [bike_name, stock, price]);
    return users[0].id;
};

module.exports = {
    getBikes,
    getBikeById,
    deleteBike,
    updateBike,
    addBike
}