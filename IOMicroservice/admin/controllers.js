const Router = require('express').Router();

const {
    getBikes,
    getBikeById,
    deleteBike,
    updateBike,
    addBike
} = require('./services.js');

Router.get('/', async (req, res) => {
    const bikes = await getBikes();
    res.json(bikes);
});

Router.get('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const bike = await getBikeById(id);
    res.json(bike);
});

Router.put('/:id', async (req, res) => {
    const {
        bike_name,
        price,
        stock
    } = req.body;

    const {
        id
    } = req.params;

    const resId = await updateBike(id, bike_name, price, stock);
    res.json(resId);
});

Router.post('/', async (req, res) => {
    const {
        bike_name,
        price,
        stock
    } = req.body;

    const resId = await addBike(bike_name, price, stock);
    res.json(resId);
});

Router.delete('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const bike = await deleteBike(id);
    res.json(bike);
});

module.exports = Router;