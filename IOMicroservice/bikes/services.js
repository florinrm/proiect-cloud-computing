const {
    query
} = require('../data');

const getBikes = async () => {
    console.info(`Getting all bikes ...`);

    const bikes = await query("SELECT id, bike_name, stock, price FROM bikes");

    return bikes;
};

const getBikeById = async (id) => {
    console.info(`Getting bike ${id} ...`);

    const books = await query("SELECT bike_name, stock, price FROM bikes WHERE id = $1", [id]);

    return books[0];
};

const buyBikeById = async (id, user) => {
    console.info(`Buying bike with id ${id} by ${user}`);

    const bikeToBuy = await query("SELECT bike_name, stock, price FROM bikes WHERE id = $1", [id]);
    const stock = bikeToBuy[0].stock - 1;
    const price = bikeToBuy[0].price;

    console.info(stock);
    console.info(price);

    const userCash = await query("SELECT * FROM users WHERE username = $1", [user]);
    console.info(userCash);
    let cash = userCash[0].cash;
    cash -= price;

    await query("UPDATE users SET cash = $1 WHERE username = $2", [cash, user]);

    const bikes = await query("UPDATE bikes SET stock = $1 where id = $2", [stock, id]);
    return bikes[0];
};

module.exports = {
    getBikes,
    getBikeById,
    buyBikeById
}