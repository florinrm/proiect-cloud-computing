const Router = require('express').Router();

const BikesController = require('./bikes/controllers.js');
const UsersController = require('./users/controllers.js');
const AdminController = require('./admin/controllers.js');

Router.use('/bikes', BikesController);
Router.use('/users', UsersController);
Router.use('/admin', AdminController);

module.exports = Router;