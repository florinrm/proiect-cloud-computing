const Router = require('express').Router();

const BikesRoute = require('./bikes.js');
const UsersRoute = require('./users.js');
const AdminRoute = require('./admin.js');

Router.use('/bikes', BikesRoute);
Router.use('/users', UsersRoute);
Router.use('/admin', AdminRoute);

module.exports = Router;
