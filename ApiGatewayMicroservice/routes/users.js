const Router = require("express").Router();

const { 
	sendRequest 
} = require("../http-client");

Router.get("/", async (req, res) => {
	console.info(`Forwarding request for get users ...`);

	const getUsersRequest = {
		url: `http://${process.env.USERS_SERVICE_API_ROUTE}`,
	};

	const users = await sendRequest(getUsersRequest);

	res.json(users);
});

Router.get("/:id", async (req, res) => {
	const { 
		id 
	} = req.params;

	console.info(`Forwarding request for get user ${id} ...`);

	const getUserIdRequest = {
		url: `http://${process.env.USERS_SERVICE_API_ROUTE}/${id}`,
	};

	const user = await sendRequest(getUserIdRequest);

	res.json(user);
});

Router.put("/:id", async (req, res) => {
	const { 
		username,
		password,
		role,
		cash
	} = req.body;

	const { 
		id 
	} = req.params;

	console.info(`Forwarding request for update user ${id} ...`);

	const putUserRequest = {
		url: `http://${process.env.USERS_SERVICE_API_ROUTE}/${id}`,
		method: "PUT",
		data: {
			username,
			password,
			role,
			cash
		},
	};

	const resId = await sendRequest(putUserRequest);

	res.json({ 
		resId 
	});
});

Router.post("/", async (req, res) => {
	const { 
		username,
		password,
		role,
		cash
	} = req.body;

	console.info(`Forwarding request for add user ${username} with ${cash} $`);

	const postUserRequest = {
		url: `http://${process.env.USERS_SERVICE_API_ROUTE}`,
		method: "POST",
		data: {
			username,
			password,
			role,
			cash
		}
	};

	const resId = await sendRequest(postUserRequest);

	res.json({ 
		resId 
	});
});

Router.post("/login", async (req, res) => {
	const {
		username,
		password
	} = req.body;

	console.info(`Forwarding request for login user ${username} ...`);

	const loginUserRequest = {
		url: `http://${process.env.USERS_SERVICE_API_ROUTE}/login`,
		method: "POST",
		data: {
			username,
			password
		}
	};

	const resId = await sendRequest(loginUserRequest);

	res.json({ 
		resId 
	});
});

Router.delete("/:id", async (req, res) => {
	const { 
		id 
	} = req.params;

	console.info(`Forwarding request for delete user ${id} ...`);

	const deleteUserRequest = {
		url: `http://${process.env.USERS_SERVICE_API_ROUTE}/${id}`,
		method: "DELETE"
	};

	const resId = await sendRequest(deleteUserRequest);

	res.json({ 
		resId 
	});
});

module.exports = Router;
