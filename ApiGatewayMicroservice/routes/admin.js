const Router = require("express").Router();

const TokenService = require('../SecurityUtils/Jwt');
const {
	authorizeRoles
} = require('../SecurityUtils/Roles');
const { 
  	sendRequest 
} = require("../http-client");

Router.get("/", TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res) => {
	console.info(`Forwarding request for get bikes`);

	const getBooksRequest = {
		url: `http://${process.env.ADMIN_SERVICE_API_ROUTE}`,
	};

	const books = await sendRequest(getBooksRequest);

	res.json(books);
});

Router.get("/:id", TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res) => {
  	const { 
    	id 
  	} = req.params;

  	console.info(`Forwarding request for get bike ${id}`);

  	const getBookIdRequest = {
    	url: `http://${process.env.ADMIN_SERVICE_API_ROUTE}/${id}`,
  	};

  	const book = await sendRequest(getBookIdRequest);

  	res.json(book);
});

Router.put("/:id", TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res) => {
	const { 
		bike_name,
        price,
        stock
  	} = req.body;

  	const { 
    	id 
  	} = req.params;

  	console.info(`Forwarding request for updating bike ${id} with name ${bike_name}, with price ${price} and stock of ${stock}`);

  	const putBikeRequest = {
    	url: `http://${process.env.ADMIN_SERVICE_API_ROUTE}/${id}`,
    	method: "PUT",
    	data: {
			bike_name,
            price,
            stock
    	},
  	};

  	const resId = await sendRequest(putBikeRequest);

  	res.json({ 
    	resId 
  	});
});

Router.post("/", TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res) => {
	const { 
		bike_name,
        price,
        stock
  	} = req.body;

  	console.info(`Forwarding request for adding bike with name ${bike_name}, with price ${price} and stock of ${stock}`);

  	const postBikeRequest = {
    	url: `http://${process.env.ADMIN_SERVICE_API_ROUTE}`,
    	method: "POST",
    	data: {
			bike_name,
            price,
            stock
    	},
  	};

  	const resId = await sendRequest(postBikeRequest);

  	res.json({ 
    	resId 
  	});
});

Router.delete("/:id", TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res) => {
  	const { 
    	id 
  	} = req.params;

  	console.info(`Forwarding request for deleting bike ${id}`);

  	const deleteBikeRequest = {
    	url: `http://${process.env.ADMIN_SERVICE_API_ROUTE}/${id}`,
    	method: "DELETE"
  	};

  	const resId = await sendRequest(deleteBikeRequest);

  	res.json({ 
    	resId 
  	});
});

module.exports = Router;
