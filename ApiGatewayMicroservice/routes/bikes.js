const Router = require("express").Router();

const TokenService = require('../SecurityUtils/Jwt');
const {
	authorizeRoles
} = require('../SecurityUtils/Roles');
const { 
  sendRequest 
} = require("../http-client");

Router.get("/", TokenService.authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res) => {
  console.info(`Forwarding request for get bikes ...`);

  const getBooksRequest = {
    url: `http://${process.env.BIKES_SERVICE_API_ROUTE}`,
  };

  const books = await sendRequest(getBooksRequest);

  res.json(books);
});

Router.get("/:id", TokenService.authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res) => {
  const { 
    id 
  } = req.params;

  console.info(`Forwarding request for get bike ${id} ...`);

  const getBookIdRequest = {
    url: `http://${process.env.BIKES_SERVICE_API_ROUTE}/${id}`,
  };

  const book = await sendRequest(getBookIdRequest);

  res.json(book);
});

Router.put("/:id", TokenService.authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res) => {
  const { 
    username
  } = req.body;

  const { 
    id 
  } = req.params;

  console.info(`Forwarding request for buy bike ${id} ...`);

  const putBikeRequest = {
    url: `http://${process.env.BIKES_SERVICE_API_ROUTE}/${id}`,
    method: "PUT",
    data: {
      username
    },
  };

  const resId = await sendRequest(putBikeRequest);

  res.json({ 
    resId 
  });
});

module.exports = Router;
