const {
    sendRequest
} = require('./http-client');

const getBikes = async () => {
    console.info(`Sending request to IO for all bikes`);
    
    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/bikes`
    }

    const bikes = await sendRequest(options);

    return bikes;
};

const getBikeById = async (id) => {
    console.info(`Sending request to IO for bike ${id}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/bikes/${id}`
    }

    const bike = await sendRequest(options);

    return bike;
};

const buyBikeById = async (id, username) => {
    console.info(`Sending request to IO for buying bike ${id}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/bikes/${id}`,
        method: 'PUT',
        data: {
            username,
        }
    }

    const resId = await sendRequest(options);
    return resId;
};  

module.exports = {
    getBikes,
    getBikeById,
    buyBikeById
}