const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const {
    getBikeById,
    getBikes,
    buyBikeById
} = require('./services.js');

Router.get('/', async (req, res) => {
    
    const books = await getBikes();

    res.json(books);
});

Router.get('/:id', async (req, res) => {

    const {
        id
    } = req.params;

    const book = await getBikeById(id);

    res.json(book);
});

Router.put('/:id', async (req, res) => {

    const {
        username,
    } = req.body;

    const {
        id
    } = req.params;

    if (!username) {
        throw new ServerError('No title provided!', 400);
    }

    const resId = await buyBikeById(id, username);

    res.json(resId);
});

module.exports = Router;