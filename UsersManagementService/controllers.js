const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const {
    getUsers,
    getUserById,
    createUser,
    deleteUser,
    updateUser,
	loginUser
} = require('./services.js');

Router.get('/', async (req, res) => {
    const users = await getUsers();
    res.json(users);
});

Router.get('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const user = await getUserById(id);
    res.json(user);
});

Router.put('/:id', async (req, res) => {

    const {
        username,
		password,
		role,
        cash
    } = req.body;

    const {
        id
    } = req.params;

    if (!username) {
        throw new ServerError('No username provided!', 400);
    }

	if (!password) {
        throw new ServerError('No password provided!', 400);
	}

	if (!role) {
        throw new ServerError('No role provided!', 400);
	}

    if (!cash) {
        throw new ServerError('No cash provided', 400)
    }

    const resId = await updateUser(id, username, password, role, cash);
    res.json(resId);
});

Router.post('/', async (req, res) => {
    const {
        username,
		password,
		role,
        cash
    } = req.body;

    if (!username) {
        throw new ServerError('No username provided!', 400);
    }

	if (!password) {
        throw new ServerError('No username provided!', 400);
	}
	
	if (!role) {
		throw new ServerError('No role provided!', 400);
	}

    if (!cash) {
        throw new ServerError('No cash provided', 400)
    }

    const resId = await createUser(username, password, role, cash);
    res.json(resId);
});

Router.post('/login', async (req, res) => {
	const {
		username,
		password
	} = req.body;

	const token = await loginUser(username, password);

    res.json(token);
});

Router.delete('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const resId = await deleteUser(id);
    res.json(resId);
});

module.exports = Router;
