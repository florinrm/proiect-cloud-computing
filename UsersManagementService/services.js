const {
    sendRequest
} = require('./http-client');

const getUsers = async () => {
    console.info(`Sending request to IO for all users`);
    
    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users`
    }

    const users = await sendRequest(options);
    return users;
};

const getUserById = async (id) => {
    console.info(`Sending request to IO for get user ${id}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/${id}`
    }

    const user = await sendRequest(options);
    return user;
};

const updateUser = async (id, username, password, role, cash) => {
    console.info(`Sending request to IO for update user ${id}, with name ${username} and ${cash} $`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/${id}`,
        method: 'PUT',
        data: {
            username,
			password,
			role,
            cash
        }
    }

    const resId = await sendRequest(options);
    return resId;
};

const deleteUser = async (id) => {
    console.info(`Sending request to IO for delete user ${id}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/${id}`,
        method: 'DELETE'
    }

    const resId = await sendRequest(options);
    return resId;
}; 

const createUser = async (username, password, role, cash) => {
    console.info(`Sending request to IO for create user with name ${username} and ${cash} $`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users`,
        method: 'POST',
        data: {
            username,
			password,
			role,
            cash
        }
    }

    const resId = await sendRequest(options);
    return resId;
};

const loginUser = async (username, password) => {
    console.info(`Sending request to IO for login user with name ${username}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/login`,
        method: 'POST',
        data: {
            username,
			password
        }
    }

    const resId = await sendRequest(options);
    return resId;
};  

module.exports = {
    getUsers,
    getUserById,
    createUser,
    deleteUser,
    updateUser,
	loginUser
}
