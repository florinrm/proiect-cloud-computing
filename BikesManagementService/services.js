const {
    sendRequest
} = require('./http-client');

const getBikes = async () => {
    console.info(`Sending request to IO for all bikes`);
    
    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/admin`
    }

    const users = await sendRequest(options);
    return users;
};

const getBikeById = async (id) => {
    console.info(`Sending request to IO for get user ${id}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/admin/${id}`
    }

    const user = await sendRequest(options);
    return user;
};

const updateBike = async (id, bike_name, price, stock) => {
    console.info(`Sending request to IO for update bike ${id}, with name ${bike_name}, ${price} $ and no. of ${stock}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/admin/${id}`,
        method: 'PUT',
        data: {
            bike_name,
            price,
            stock
        }
    }

    const resId = await sendRequest(options);
    return resId;
};  

const deleteBike = async (id) => {
    console.info(`Sending request to IO for delete bike ${id}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/admin/${id}`,
        method: 'DELETE'
    }

    const resId = await sendRequest(options);
    return resId;
}; 

const addBike = async (bike_name, price, stock) => {
    console.info(`Sending request to IO for create bike with name ${bike_name}, ${price} $ and no. of ${stock}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/admin`,
        method: 'POST',
        data: {
            bike_name, 
            price, 
            stock
        }
    }

    const resId = await sendRequest(options);
    return resId;
};

module.exports = {
    getBikes,
    getBikeById,
    updateBike,
    deleteBike,
    addBike
}