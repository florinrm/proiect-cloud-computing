const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const {
    getBikes,
    getBikeById,
    deleteBike,
    updateBike,
    addBike
} = require('./services.js');

Router.get('/', async (req, res) => {
    const users = await getBikes();
    res.json(users);
});

Router.get('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const user = await getBikeById(id);
    res.json(user);
});

Router.put('/:id', async (req, res) => {

    const {
        bike_name, 
        price, 
        stock
    } = req.body;

    const {
        id
    } = req.params;

    if (!bike_name) {
        throw new ServerError('No bike_name provided!', 400);
    }

    if (!price) {
        throw new ServerError('No price provided', 400)
    }

    if (!stock) {
        throw new ServerError('No stock provided', 400)
    }

    const resId = await updateBike(id, bike_name, price, stock);
    res.json(resId);
});

Router.post('/', async (req, res) => {
    const {
        bike_name, 
        price, 
        stock
    } = req.body;

    if (!bike_name) {
        throw new ServerError('No bike_name provided!', 400);
    }

    if (!price) {
        throw new ServerError('No price provided', 400)
    }

    if (!stock) {
        throw new ServerError('No stock provided', 400)
    }

    const resId = await addBike(bike_name, price, stock);
    res.json(resId);
});

Router.delete('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const resId = await deleteBike(id);
    res.json(resId);
});

module.exports = Router;